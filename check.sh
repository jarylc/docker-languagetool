#!/bin/bash

apk add curl jq

[[ ! -f EXISTING_LGT ]] || touch EXISTING_LGT
EXISTING_LGT=$(cat EXISTING_LGT)
echo "Existing LanguageTool: ${EXISTING_LGT}"

if [[ -n $OVERWRITE_LGT ]]; then
  echo "Overwriting QBT: $OVERWRITE_LGT"
  LATEST_LGT=$OVERWRITE_LGT
else
  # look through the last 5 tags for a proper version number
  for i in $(seq 5); do
    LATEST_LGT=$(curl -ks https://api.github.com/repos/languagetool-org/languagetool/git/refs/tags | jq -r ".[$((0 - i))].ref" | cut -d'/' -f3)
    echo "$LATEST_LGT" | grep -qE '^v?\d+\.\d+(.\d+)?$' && break
  done
  echo "Latest LanguageTool: ${LATEST_LGT}"
fi

if [[ (-n "${LATEST_LGT}" && "${LATEST_LGT}" != "${EXISTING_LGT}") ]]; then
  mv build.template.yml build.yml
  sed -i "s \$LATEST_LGT ${LATEST_LGT} g" 'build.yml'

  echo "Building..."
fi
